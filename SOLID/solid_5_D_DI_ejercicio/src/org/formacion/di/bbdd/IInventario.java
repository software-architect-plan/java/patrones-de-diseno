package org.formacion.di.bbdd;

public interface IInventario {
    int numeroProductos(String tienda, String producto);
}
