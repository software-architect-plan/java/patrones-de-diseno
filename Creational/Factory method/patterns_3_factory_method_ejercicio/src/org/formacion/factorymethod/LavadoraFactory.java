package org.formacion.factorymethod;

// clase base que usara el cliente para crear cualquier tipo de lavadora.
public abstract class LavadoraFactory {

    // proces de construcciòn comun
    public Lavadora crear() {
        Lavadora lavadora = creaLavadora();
        lavadora.ponerMandos();
        lavadora.ponerTambor();

        return lavadora;
    }

    // Proceso de construcciòn especefico de cada tipo de lavadora
    // Permite que el tipo devuelto sea un subtipo de Lavadora (y por tanto,
    // las invocaciones al metodo crear() devuelvan tipo diferentes de lavadoras)
    protected abstract  Lavadora creaLavadora();
}
