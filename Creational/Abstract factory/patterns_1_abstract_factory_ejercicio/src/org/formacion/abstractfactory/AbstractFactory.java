package org.formacion.abstractfactory;

public interface AbstractFactory {
    Preguntas getPreguntas();
    Saludos getSaludos();
}
