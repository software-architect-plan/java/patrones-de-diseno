package org.formacion.proxy;

public class CuentaNueva implements Cuenta {

    private Cuenta original;

    public CuentaNueva(String cliente) {
        this.original = new CuentaNormal(cliente);
    }
    @Override
    public String getCliente() {
        return original.getCliente();
    }

    @Override
    public int getCantidad() {
        return original.getCantidad();
    }

    @Override
    public void movimiento(int importe) {
        int resultado = getCantidad() + importe;
        if (original.getCantidad() + importe >=0){
            original.movimiento(resultado);
        }
    }
}
