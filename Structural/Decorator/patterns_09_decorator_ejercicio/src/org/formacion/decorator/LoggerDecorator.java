package org.formacion.decorator;

import java.util.List;

public class LoggerDecorator implements BaseDatos {

	private final Logger logger; // para registrar los logs
	private final BaseDatos impl; // implementacion real del sistema de base de datos

	public LoggerDecorator(Logger logger, BaseDatos impl) {
		this.logger = logger;
		this.impl = impl;
	}
	@Override
	public void inserta(String registro) {
		// implementar
		logger.add("inserta " + registro);
		impl.inserta(registro);
	}

	@Override
	public List<String> registros() {
		// implementar
		logger.add("lectura");
		return impl.registros();
	}

	

}
